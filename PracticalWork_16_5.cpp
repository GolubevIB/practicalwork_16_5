// PracticalWork_16_5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>


int main()
{
    const int N = 6;
    int array[N][N] = {};
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << "  ";
        }
        std::cout << "\n";
    }
    std::cout << "\n\n";
    
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int Day = buf.tm_mday;
    int Row = Day % N;
    int Sum = 0;

    for (int i = 0; i < N; i++)
    {
        Sum += array[Row][i];
    }
    std::cout << "Sum elements of " << Row << " row = " << Sum << "\n\n";


}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
